import React from 'react';

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { withStyles } from '@material-ui/core/styles';

import logo from '../../images/logo.png'

const StyledButton = withStyles({
  root: {
    background: '#61B2AA',
    borderRadius: 30,
    border: 0,
    color: 'white',
    width: '40%',
    padding: '10px 30px 10px 30px',
    boxShadow: '0 3px 5px 2px rgb(176,176,176)',
    alignSelf: 'center',
  },
  label: {
    textTransform: 'uppercase',
  },
})(Button);

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFF',
    width: '90%',
    alignSelf: 'center',
    padding: '50px 30px 200px 30px',
    boxShadow: '0 15px 25px 10px rgb(232,232,232)',
    borderRadius: 30
  },
  logo: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  input: {
    flexDirection: 'column',
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function LoginPage() {
  const classes = useStyles();
  return (
    <Container component="main" maxWidth="fixed" className={classes.root}>
      <CssBaseline />
        <div className={classes.logo}>
          <img src={logo}/>
        </div>
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Log In
        </Typography>
        <form className={classes.form} noValidate>
          <Grid className={classes.input}>
          <Grid item>
                <Typography variant="body2">
                  Email
                </Typography>
            </Grid>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
            />
          </Grid>
          <Grid item>
              <Typography variant="body2">
                Password
              </Typography>
          </Grid>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Type your password"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <Grid item>
              <Link href="#" style={{ color: '#A1A1A4' }} variant="body2">
                {"Forgot password?"}
              </Link>
          </Grid>
        </form>
          <StyledButton
            type="submit"
            variant="contained"
            className={classes.submit}
          >
            <Link style={{ color: '#FFF' }} to="/status">Login</Link>
          </StyledButton>
      </div>
    </Container>
  );
}