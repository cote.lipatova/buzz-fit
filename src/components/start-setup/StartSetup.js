import React from 'react';

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { withStyles } from '@material-ui/core/styles';

import logo from '../../images/logo.png'
import welcome from '../../images/welcome.png'

const StyledButton = withStyles({
  root: {
    background: '#61B2AA',
    borderRadius: 30,
    border: 0,
    color: 'white',
    width: '40%',
    padding: '10px 30px 10px 30px',
    boxShadow: '0 3px 5px 2px rgb(176,176,176)',
    alignSelf: 'center',
  },
  label: {
    textTransform: 'uppercase',
  },
})(Button);

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: '100%'
  },
  paper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20
  },
  logo: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30
  },
  form: {
    margin: 30
  },
  input: {
    flexDirection: 'column',
  },
}));

export default function StartSetup() {
  const classes = useStyles();
  return (
    <Container component="main" maxWidth="fixed" className={classes.root}>
      <CssBaseline />
        <Grid className={classes.logo}>
                <img src={logo}/>
        </Grid>
        <Grid className={classes.paper}>
            <Typography component="h1" variant="h5">
            Set Up Your <br />
            Buzz Fit Box
            </Typography>
        </Grid>
        <Grid className={classes.logo}>
            <img src={welcome}/>
        </Grid>
        <Grid xs={12} className={classes.paper}>
            <Typography variant="body1" gutterBottom>
            1. Before you start, make sure you are at the location you are installing the device.
            </Typography>
        </Grid>
        <Grid className={classes.paper}>
            <Typography variant="body1" gutterBottom>
            2. Enter the unique code you see on your screen to begin.
            </Typography>
        </Grid>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="dense"
            fullWidth
            name="password"
            label="P90TDJ"
            type="password"
            id="password"
          />
        </form>
          <StyledButton
            type="submit"
            variant="contained"
            className={classes.submit}
          >
            <Link style={{ color: '#FFF' }} to="/status">continue</Link>
          </StyledButton>
    </Container>
  );
}