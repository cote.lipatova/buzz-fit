import React from 'react';

import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';

import Cards from './Cards';
import "../../styles/common.sass";
import logo from '../../images/logo.png'

  
const useStyles = makeStyles({
    root: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    paper: {
        textAlign: 'center',
        color: "textSecondary"
    },
    tabs: {
        display: 'flex',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 20,
    },
    pos: {
        marginBottom: 12,
    },
    h1: {
        fontSize: 30,
        fontWeight: 600,
        marginBottom: 30
    },
    logo: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    }
});

const CustTabs = withStyles({
    root: {
      borderBottom: '1px solid #61B2AA',
    },
    indicator: {
      backgroundColor: '#61B2AA',
    },
    tabs: {
        display: 'flex',
        alignItems: 'center'
    }
  })(Tabs);
  const CustTab = withStyles((theme) => ({
    root: {
        justifyContent: 'space-around',
        alignItems: 'space-around',
      textTransform: 'none',
      minWidth: 72,
      fontWeight: theme.typography.fontWeightRegular,
      marginRight: theme.spacing(4),
      fontFamily: [
        'Roboto',
      ].join(','),
      '&:hover': {
        color: '#61B2AA',
        opacity: 1,
      },
      '&$selected': {
        color: '#61B2AA',
        fontWeight: theme.typography.fontWeightMedium,
      },
      '&:focus': {
        color: '#61B2AA',
      },
    },
    selected: {},
  }))((props) => <Tab disableRipple {...props} />);

export default function Status() {
    const [value, setValue] = React.useState('boxes');

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={11}>
                    <div className={classes.logo}>
                        <img src={logo}/>
                    </div>
                </Grid>
                <Grid item xs={11}>
                    <Paper className={classes.paper} elevation={0}>
                        <Typography className={classes.h1}>
                            Welcome DrX
                        </Typography>
                    </Paper>
                </Grid>
                <Grid className={classes.tabs} item xs={11}>
                    <CustTabs
                        value={value}
                        onChange={handleChange}
                    >
                        <CustTab value="boxes" label="My Boxes" />
                        <CustTab value="orders" label="Order History" />
                    </CustTabs>
                </Grid>
                <Grid item xs={11}>
                    <Paper className={classes.paper} elevation={0}>
                        <Cards />
                    </Paper>
                </Grid>
            </Grid>
        </div>
    );
}