import React, { useState, useEffect } from 'react';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import "../../styles/common.sass";
import data from '../../data/boxes.json';

  
const useStyles = makeStyles({
    root: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    paper: {
        textAlign: 'center',
        color: "textSecondary"
    },
    card: {
      width: '90%',
      borderRadius: 20,
      margin: 15
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
});

export default function Cards() {
    const classes = useStyles();
    console.log(data)
    return (
            <div className={classes.root}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Paper className={classes.paper} elevation={0}>
                        {data.boxes.map(el => (
                            <Card className={classes.card} key={el.id}>
                                <CardContent>
                                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                                    {el.name}
                                    </Typography>
                                    <Typography className={classes.pos} color="textSecondary">
                                    {el.descr}
                                    </Typography>
                                    <Typography className={classes.pos} color="textSecondary">
                                    {el.status}
                                    </Typography>
                                    <Typography className={classes.pos} color="textSecondary">
                                    {el.device}
                                    </Typography>
                                    <Typography className={classes.pos} color="textSecondary">
                                    {el.IP}
                                    </Typography>
                                </CardContent>
                            </Card>
                             ))}
                        </Paper>
                    </Grid>
                </Grid>
                
            </div>
    );
}