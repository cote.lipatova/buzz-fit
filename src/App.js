import './App.scss';
import LoginPage from './components/login/LoginPage';
import Status from './components/status/Status';

function App() {
  return (
    <div className="App">
      <Status />
    </div>
  );
}

export default App;
