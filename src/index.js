import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import LoginPage from './components/login/LoginPage'
import Status from './components/status/Status'
import StartSetup from './components/start-setup/StartSetup'

ReactDOM.render((
    <BrowserRouter>
        <Switch>
            <Route exact path='/' component={LoginPage}/>
            <Route exact path='/status' component={Status}/>
            <Route exact path='/start-setup' component={StartSetup}/>
        </Switch>
    </BrowserRouter>
  ), document.getElementById('root'))

